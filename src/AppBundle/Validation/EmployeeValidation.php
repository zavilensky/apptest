<?php

namespace AppBundle\Validation;

class EmployeeValidation
{
    private $employee;

    public function __construct($employee)

    {
       $this->employee = $employee;
    }

    public function validate()
    {
        $error = [];

        if(empty($this->employee->getFirstName())){
            $error['firstName'] = true;
        }

        if(preg_match ( '/[0-9]+/' , $this->employee->getFirstName())){
            $error['firstName'] = true;
        }

        if(empty($this->employee->getLastName())){
            $error['lastName'] = true;
        }

        if(preg_match ( '/[0-9]+/' , $this->employee->getLastName())){
            $error['lastName'] = true;
        }

        if(preg_match ( '/[0-9]+/' , $this->employee->getMiddleName())){
            $error['middleName'] = true;
        }

        if(preg_match ( '/![0-9]+/' , $this->employee->getSalary())){
            $error['salary'] = $this->employee->getSalary();
        }

        if(0 == $this->employee->getDepartments()->count()){
            $error['department'] = true;
        }

        if (count($error) == 0){
            return true;
        } else {
            return $error;
        }
    }

}
