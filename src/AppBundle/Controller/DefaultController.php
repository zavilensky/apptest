<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $departments = $em->getRepository('AppBundle:Department')->findAll();
        $employes = $em->getRepository('AppBundle:Employee')->findAll();

        $tdStringArray = '';
        $employesArray = '';
        foreach ($employes as $employe){
            foreach ($departments as $department) {
                $result = false;

                foreach ($employe->getDepartments() as $valye) {
                    if ($valye->getId() == $department->getId()) {
                        $result = 'checked';
                        break;
                    }
                }

                $tdString [] = $result;
            }

            $employesArray [] = $employe->getFirstName().' '.$employe->getLastName();
            $tdStringArray [] = $tdString;
            unset($tdString);
        }

        return $this->render('app/grid.html.twig', array(
            'departments' => $departments,
            'tdStringArray' => $tdStringArray,
            'employesArray' => $employesArray,
        ));
    }
}
