<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Validation\EmployeeValidation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EmployeeController extends Controller
{
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $employees = $entityManager->getRepository('AppBundle:Employee')->findAll();

        $departmentEmployees = [];
        foreach ($employees as $employee){
            $departmentEmployees[$employee->getId()] = $this->departmentEmployees($employee);
        }

        $employee = new Employee();
        $formNew = $this->createForm('AppBundle\Form\EmployeeType', $employee);

        $formNew->handleRequest($request);

        if ($formNew->isSubmitted() && $formNew->isValid()) {
            $validation = new EmployeeValidation($employee);
            $result = $validation->validate();

            if($result === true){
                $em = $this->getDoctrine()->getManager();
                $em->persist($employee);
                $em->flush();

                $departmentEmployees[$employee->getId()] = $this->departmentEmployees($employee);

                return $this->render('app/employee/new_show.html.twig', array(
                    'employee' => $employee,
                    'departmentEmployees' => $departmentEmployees,
                ));
            } else {
                $result = json_encode($result);
                return $this->json(array('error' => $result));
            }
        }

        return $this->render('app/employee/index.html.twig', array(
            'employees' => $employees,
            'departmentEmployees' => $departmentEmployees,
            'formNew' => $formNew->createView(),
        ));
    }

    public function editAction(Request $request, Employee $employee)
    {
        $editForm = $this->createForm('AppBundle\Form\EmployeeType', $employee, array (
            'action' => $this -> generateUrl ( 'employee_edit' , array(
                    'id' => $employee->getId(),
                )
            )));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $validation = new EmployeeValidation($employee);
            $result = $validation->validate();

            if($result === true){
                $this->getDoctrine()->getManager()->flush();

                $departmentEmployees[$employee->getId()] = $this->departmentEmployees($employee);

                return $this->render('app/employee/show.html.twig', array(
                    'employee' => $employee,
                    'departmentEmployees' => $departmentEmployees,
                ));
            } else {
                $result = json_encode($result);
                return $this->json(array('error' => $result));
            }
        }

        return $this->render('app/employee/edit.html.twig', array(
            'formNew' => $editForm->createView(),
            'id' => $employee->getId(),
        ));
    }

    public function deleteAction(Request $request, Employee $employee)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($employee);
        $em->flush();

        return $this->json(array('result' => true));
    }

    private function departmentEmployees($employee)
    {
        $departmentEmployees = '';

        foreach ($employee->getDepartments() as $department){
            $departmentEmployees .= $department.',';
        }

        return $departmentEmployees;
    }

}
