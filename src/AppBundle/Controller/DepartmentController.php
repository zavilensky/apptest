<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DepartmentController extends Controller
{
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $departments =  $entityManager->getRepository('AppBundle:Department')->findAll();

        foreach ($departments as $department){
            $result = $this->selectQuery($department);

            $maxSalary[$department->getId()] = $result['maxSalary'];
            $numberEmployees [$department->getId()]  = $result['numberEmployees'];
        }



        $department = new Department();
        $formNew = $this->createForm('AppBundle\Form\DepartmentType', $department);

        $formNew->handleRequest($request);

        if ($formNew->isSubmitted() && $formNew->isValid()) {

            $resultValid = $this->validate($department);

            if($resultValid === true){
                $em = $this->getDoctrine()->getManager();
                $em->persist($department);
                $em->flush();

                $result = $this->selectQuery($department);

                $maxSalary[$department->getId()] = $result['maxSalary'];
                $numberEmployees [$department->getId()]  = $result['numberEmployees'];

                return $this->render('app/department/new_show.html.twig', array(
                    'department' => $department,
                    'maxSalary' => $maxSalary,
                    'numberEmployees' => $numberEmployees,
                ));
            } else {
                $resultValid = json_encode($resultValid);
                return $this->json(array('error' => $resultValid));
            }
        }

        return $this->render('app/department/index.html.twig', array(
            'departments' => $departments,
            'maxSalary' => $maxSalary,
            'numberEmployees' => $numberEmployees,
            'formNew' => $formNew->createView(),
        ));
    }



    public function editAction(Request $request, Department $department)
    {
        $editForm = $this->createForm('AppBundle\Form\DepartmentType', $department, array (
            'action' => $this -> generateUrl ( 'department_edit' , array(
                    'id' => $department->getId(),
                )
            )));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $resultValid = $this->validate($department);

            if($resultValid === true){
                $this->getDoctrine()->getManager()->flush();

                $result = $this->selectQuery($department);

                $maxSalary[$department->getId()] = $result['maxSalary'];
                $numberEmployees [$department->getId()]  = $result['numberEmployees'];

                return $this->render('app/department/show.html.twig', array(
                    'department' => $department,
                    'maxSalary' => $maxSalary,
                    'numberEmployees' => $numberEmployees,
                ));
            } else {
                $resultValid = json_encode($resultValid);
                return $this->json(array('error' => $resultValid));
            }
        }

        return $this->render('app/department/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
            'id' => $department->getId(),
        ));
    }

    public function deleteAction(Request $request, Department $department)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT count(employee.id) FROM
                AppBundle:Employee employee
                JOIN employee.departments  department
                WHERE department.id = :id'
        )->setParameter('id', $department->getId());

        $result = $query->getSingleScalarResult();

        if ($result == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($department);
            $em->flush();

            return $this->json(array('result' => true));
        } else {
            return $this->json(array('result' => false));
        }

    }


    private function selectQuery($department)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT max(employee.salary) FROM
                AppBundle:Employee employee
                JOIN employee.departments  department
                WHERE department.id = :id'
        )->setParameter('id', $department->getId());


        $result = $query->getSingleScalarResult();
        $maxSalary = $result;

        $query = $entityManager->createQuery(
            'SELECT count(employee.id) FROM
                AppBundle:Employee employee
                JOIN employee.departments  department
                WHERE department.id = :id'
        )->setParameter('id', $department->getId());

        $result = $query->getSingleScalarResult();
        $numberEmployees = $result;
        return [
            'numberEmployees' => $numberEmployees,
            'maxSalary' => $maxSalary,
        ];
    }

    private function validate($department)
    {
        $error = [];

        if(empty($department->getName())){
            $error['name'] = true;
        }

        if(preg_match ( '/[0-9]+/' , $department->getName())){
            $error['name'] = true;
        }

        if (count($error) == 0){
            return true;
        } else {
            return $error;
        }
    }

}
